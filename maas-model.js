/*
 * Creating an openstack test cluster.
 */

function addDisks() {
  disks = [];
  for (i = 0; i < 1; i++) {
    disks.push({
      "size": "32G",
      "dev": "vd" + String.fromCharCode("c".charCodeAt() + i),
      "bus": "virtio",
    })
  }
  return disks
}

function Controller(name) {
  return {
    name: name,
    image: "user/pulwar",
    cpu: { cores: 1 },
    memory: { capacity: GB(4) },
  };
}

function AllInOne(name) {
  return {
    name: name,
    disks: addDisks(),
    image: "user/pulwar",
    defaultnic: 'e1000',
    cpu: { cores: 2 },
    memory: { capacity: GB(8) },
    dedicateddisk: true,
    diskcache: "unsafe",
    defaultdisktype: { dev: 'sda', bus: 'sata' },
    firmware: env.PWD+'/firmware/OVMF.fd',

  };
}

function Storage(name) {
  return {
    name: name,
    disks: addDisks(),
    image: "user/pulwar",
    cpu: { cores: 2 },
    memory: { capacity: GB(4) },
  };
}

function Compute(name) {
  return {
    name: name,
    image: "user/pulwar",
    disks: [ { "size": "32G", "dev": "vdb", "bus": "virtio" } ],
    cpu: {
      cores: 4,
      "passthru": true,
    },
    memory: { capacity: GB(4) },
  };
}

sw = {                                                
	  "name": "sw",                                       
	  "image": "cumulusvx-4.1",                               
	  "os": "linux",                                          
	  "defaultdisktype": { "bus": "virtio", "dev": "vda" },
	  "cpu": { "cores": 1 },
	  "memory": { "capacity": GB(2) },   
}

stors = {node: Range(2).map(i => Storage("stor"+i))}
comps = {node: Range(2).map(i => Compute("comp"+i))}
all = {node: Range(3).map(i => AllInOne("os"+i))}
ports = 1

topo = {
  "name": "osm",
  "nodes": [...all.node, Controller("control")],
  "switches": [sw],
  "links": [
    Link('os0', 1, 'sw', ports++, {mac: {os0: '04:70:00:01:70:1A'}, "boot": 1}),
    Link('os1', 1, 'sw', ports++, {mac: {os1: '04:70:00:01:70:1B'}, "boot": 2}),
    Link('os2', 1, 'sw', ports++, {mac: {os2: '04:70:00:01:70:1C'}, "boot": 3}),
    Link("control", 1, "sw", ports++),
    ...all.node.map(x => Link(x.name, 1, "sw", ports++)),
  ]
}
