#!/bin/bash

set -e

if [[ $EUID -ne 0 ]]; then
   echo "requires root access to build - please sudo"
   exit 1
fi

./prepare.sh

echo "Cleaning Raven."
if rvn destroy; then
	echo "Raven destroyed"
else
	exit 1
fi
if rvn -v build; then
	echo "Built Raven Topology"
else
	exit 1
fi
if rvn -v deploy; then
	echo "Deployed Raven Topology"
else
	exit 1
fi

nodes=`rvn status 2>&1 | sed 's/.*\(msg=.*\)/\1/g' | grep "  " | cut -d " " -f 3`

echo "Pinging nodes until topology is ready."
if rvn pingwait $nodes; then
	echo "Raven Topology UP"
else
	exit 1
fi
if rvn status; then
	echo "Raven Status (generate ansible)"
else
	exit 1
fi
echo "Configuring Raven Topology."
if rvn configure -v; then
	echo "Raven Status (generate ansible)"
else
	exit 1
fi

echo "Success."
