#!/bin/bash

sudo ansible-playbook -i .rvn/ansible-hosts --extra-vars="@vars.yml" switches.yml
sudo ansible-playbook -i .rvn/ansible-hosts --extra-vars="@vars.yml" control-config.yml
sudo ansible-playbook -i .rvn/ansible-hosts --extra-vars="@vars.yml" workers-config.yml

