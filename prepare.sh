#!/bin/bash

set -e

if [[ $EUID -ne 0 ]]; then
   echo "requires root access to build - please sudo"
   exit 1
fi

echo "Checking if base image exists..."
ubuntu2004=/var/rvn/img/user/pulwar
if [ ! -f "$ubuntu2004" ]; then
	sudo wget -O "$ubuntu2004" https://pulwar.isi.edu/sabres/raven-images/-/jobs/13/artifacts/raw/build/ubuntu/2004/ubuntu-2004
fi
echo "Done."
