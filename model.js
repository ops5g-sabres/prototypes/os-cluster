/*
 * Creating an openstack test cluster.
 */
function Compute(name, ncore, nmem) {
  return {
    name: name,
    image: "ubuntu-2004",
    disks: [ { "size": "32G", "dev": "vdb", "bus": "virtio" } ],
    cpu: {
      cores: ncore,
      "passthru": true,
    },
    memory: { capacity: GB(nmem) },
  };
}

sw = {                                                
	  "name": "sw",                                       
	  "image": "cumulusvx-4.1",                               
	  "os": "linux",                                          
	  "defaultdisktype": { "bus": "virtio", "dev": "vda" },
	  "cpu": { "cores": 1 },
	  "memory": { "capacity": GB(2) },   
}

all = {node: Range(3).map(i => Compute("os"+i, 2, 4))}
ports = 1

topo = {
  "name": "osm",
  "nodes": [...all.node, Compute("control", 4, 8)],
  "switches": [sw],
  "links": [
    ...all.node.map(x => Link(x.name, 1, "sw", ports++)),
    Link("control", 1, "sw", ports++),
    ...all.node.map(x => Link(x.name, 2, "sw", ports++)),
  ]
}
